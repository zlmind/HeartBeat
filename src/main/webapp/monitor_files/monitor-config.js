var lineTestConfig = [
{
    sys: "lemon",
    desc: "产品系统（朱林）",
    schema: "http://",
    host: [
	{
        ip: ["10.32.32.56", "10.32.32.105","10.32.32.154", "10.32.32.155"],
        tomcat: ["tomcat_jyq_lemon_01"],
        serverDesc: ["T", "T", "T", "T"],
        serverTitle: ["T", 	"T", "T", "T"],
        port: "10267"
    }],
    url:"/lemon-web/api/product/detail?ewogICJwcm9kdWN0SWQiOiA0LAogICJsb2FuQ3ljbGUiOiBudWxsLAogICJsb2FuTW9uZXkiOiBudWxsLAogICJjYXBpdGFsQ2hhbm5lbCI6IG51bGwKfQo="
},
{
    sys: "sakura",
    desc: "急用钱订单系统（黄飞达）",
    schema: "http://",
    host: [
    {
        ip: ["10.32.32.28", "10.32.32.42","10.32.32.161", "10.32.32.162"],
        tomcat: ["tomcat_jyq_sakura_01"],
        serverDesc: ["T", "T", "T", "T"],
        serverTitle: ["T",  "T", "T", "T"],
        port: "10278"
    }],
    url:"/sakura/view/dict/listData"
},
{
    sys: "cav",
    desc: "认证系统（朱林）",
    schema: "http://",
    host: [
    {
        ip: ["10.32.32.29", "10.32.32.43","10.32.32.164", "10.32.32.165"],
        tomcat: ["tomcat_jyq_cav_01"],
        serverDesc: ["T", "T", "T", "T"],
        serverTitle: ["T",  "T", "T", "T"],
        port: "10270"
    }],
    url:"/cav-web/api/authentication/personinfo/query?ewogICAgIm1lbWJlclNvdXJjZSI6ICJGRFMiLAogICAgIm1lbWJlcklkIjogIjEwMDI1MTY1NjkiCn0="
},
{
    sys: "vca",
    desc: "账户系统（徐晨）",
    schema: "http://",
    host: [
    {
        ip: ["10.32.32.27", "10.32.32.41","10.32.32.161", "10.32.32.162"],
        tomcat: ["tomcat_jyq_vca_01"],
        serverDesc: ["T", "T", "T", "T"],
        serverTitle: ["T",  "T", "T", "T"],
        port: "10272"
    }],
    url:"/vca-web/pdaProduct/queryDetail?ewogICJwcm9kdWN0SWQiOiA0Cn0="
},
{
    sys: "NPC",
    desc: "支付系统（汤鹏展）",
    schema: "http://",
    host: [
    {
        ip: ["10.32.32.38", "10.32.32.52"],
        tomcat: ["tomcat_pay_npc_01"],
        serverDesc: ["T", "T"],
        serverTitle: ["T",  "T"],
        port: "10151"
    }],
    url:"/payc-web/query/order/detail"
},
{
    sys: "BWA",
    desc: "支付系统（汤鹏展）",
    schema: "http://",
    host: [
    {
        ip: ["10.32.32.52", "10.32.32.66"],
        tomcat: ["tomcat_pay_bwa_01"],
        serverDesc: ["T", "T"],
        serverTitle: ["T",  "T"],
        port: "10152"
    }],
    url:"/bwa-web/remit/query/by-out-trade-no"
}
,
{
    sys: "ccf",
    desc: "财务系统（程清）",
    schema: "http://",
    host: [
    {
        ip: ["10.32.32.29", "10.32.32.43"],
        tomcat: ["tomcat_jyq_ccf_01"],
        serverDesc: ["T", "T"],
        serverTitle: ["T",  "T"],
        port: "10275"
    }],
    url:"/ccf-web/loanJyqAccountInfoList"
}
];


