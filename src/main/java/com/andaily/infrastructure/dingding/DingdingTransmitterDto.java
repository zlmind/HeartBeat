package com.andaily.infrastructure.dingding;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DingdingTransmitterDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 交互ID
	 */
	private String uuid;

	/**
	 * 系统三字码
	 */
	private String systemCode = "JYQ";

	/**
	 * 系统模块码
	 */
	private String moduleCode = "LEMON";

	/**
	 * 机器人编码
	 */
	private String[] robotCodes = { "DDT_JYQ_LEMON_2017103001" };

	/**
	 * 文本内容
	 */
	private String text;

	DingdingTransmitterDto(String text) {
		this.uuid = String.valueOf(System.currentTimeMillis());
		this.text = text;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String[] getRobotCodes() {
		return robotCodes;
	}

	public void setRobotCodes(String[] robotCodes) {
		this.robotCodes = robotCodes;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
