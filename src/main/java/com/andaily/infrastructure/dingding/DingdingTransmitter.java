package com.andaily.infrastructure.dingding;

import java.io.IOException;
import java.util.Date;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import net.sf.json.JSONObject;

public class DingdingTransmitter extends Thread implements InitializingBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(DingdingTransmitter.class);
	private static final String DINGDING_MESSAGE_URL = "http://api.ddt.fbs.blackfi.sh/ddt/api/robot/text/send";

	protected String from;
	protected String to[];
	protected String subject;
	protected String content;
	protected static boolean sendDingdingMessageUseThread;

	public static void setSendMailUseThread(boolean sendMailUseThread) {
		DingdingTransmitter.sendDingdingMessageUseThread = sendMailUseThread;
	}

	public DingdingTransmitter(String subject, String content, String... to) {
		this.to = to;
		this.subject = subject;
		this.content = content;
	}

	public void transmit() {
		if (sendDingdingMessageUseThread) {
			this.start();
		} else {
			this.run();
		}
	}

	public void run() {
		DingdingTransmitterDto dingdingContent = new DingdingTransmitterDto(content);
		JSONObject json = JSONObject.fromObject(dingdingContent);
		restRequest(json.toString());
	}

	private void restRequest(String json) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		httpClient.getParams().setParameter("http.connection.timeout", 5 * 1000);
		httpClient.getParams().setParameter("http.socket.timeout", 5 * 1000);
		httpClient.getParams().setParameter("http.protocol.content-charset", "utf-8");
		HttpPost httpPost = new HttpPost(DINGDING_MESSAGE_URL);
		httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
		httpPost.setHeader("Accept", "aplication/json");
		StringEntity entity = new StringEntity(json, "UTF-8");
		entity.setContentType("application/json");
		httpPost.setEntity(entity);
		try {
			httpClient.execute(httpPost);
		} catch (IOException e) {
			LOGGER.error("[send failure] send date on {}, json is ", new Date(), json);
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {

	}

}
