package com.andaily.domain.log.reminder;

import java.util.Arrays;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.andaily.domain.application.ApplicationInstance;
import com.andaily.domain.log.FrequencyMonitorLog;
import com.andaily.domain.log.MonitoringReminderLog;
import com.andaily.domain.shared.Application;
import com.andaily.infrastructure.DateUtils;
import com.andaily.infrastructure.STRender;
import com.andaily.infrastructure.dingding.DingdingTransmitter;

public class DingdingPerMonitoringReminderSender extends AbstractPerMonitoringReminderSender {

	private static final Logger LOGGER = LoggerFactory.getLogger(DingdingPerMonitoringReminderSender.class);
	private static final String CHANGE_NORMAL_EMAIL_SUBJECT = "Connect [%s] restore normally";
	private static final String CHANGE_NORMAL_EMAIL_CONTENT_TEMPLATE = "template/instance_change_normal_content.html";

	private static final String CHANGE_NOT_NORMAL_EMAIL_SUBJECT = "Unable to connect [%s]";
	private static final String CHANGE_NOT_NORMAL_EMAIL_CONTENT_TEMPLATE = "template/instance_change_not_normal_content.html";

	@Override
	public boolean support(FrequencyMonitorLog monitorLog) {
		return true;
	}

	@Override
	public void send(MonitoringReminderLog reminderLog, FrequencyMonitorLog monitorLog) {
		final boolean normal = monitorLog.normal();
		reminderLog.changeNormal(normal);

		String dingdingContent;
		if (normal) {
			dingdingContent = sendChangeNormalMessage(monitorLog);
		} else {
			dingdingContent = sendChangeUnNormalMessage(monitorLog);
		}

		reminderLog.emailContent(dingdingContent);

	}

	private String sendChangeNormalMessage(FrequencyMonitorLog monitorLog) {
		final ApplicationInstance instance = monitorLog.instance();

		String subject = String.format(CHANGE_NORMAL_EMAIL_SUBJECT, instance.instanceName());
		final String[] to = instance.emailAsArray();

		Map<String, Object> model = contentModel(monitorLog);
		final String content = render(subject,model);

		sendDingdingMessage(to, subject, content);
		LOGGER.debug("Sent Change-Normal Email of Instance[guid={},name={}], FrequencyMonitorLog is [{}]",
				instance.guid(), instance.instanceName(), monitorLog);

		return content;
	}

	private String sendChangeUnNormalMessage(FrequencyMonitorLog monitorLog) {
		final ApplicationInstance instance = monitorLog.instance();

		String subject = String.format(CHANGE_NOT_NORMAL_EMAIL_SUBJECT, instance.instanceName());
		final String[] to = instance.emailAsArray();
		Map<String, Object> model = contentModel(monitorLog);
		final String content = render(subject,model);

		sendDingdingMessage(to, subject, content);
		return content;
	}

	private void sendDingdingMessage(String to[], String subject, String content) {
		DingdingTransmitter transmitter = new DingdingTransmitter(subject, content, to);
		transmitter.transmit();
	}
	
	private String render(String subject,Map<String, Object> modelMap) {
		StringBuffer content = new StringBuffer();
	    String spliter = "$$";
	    content.append("时间:"+modelMap.get("time")).append(spliter);
	    content.append("主题").append(subject).append(spliter);
		content.append("监控URL:"+modelMap.get("url")).append(spliter);
		content.append("备注:"+modelMap.get("remark"));
		System.out.println(content.toString());
		return content.toString();
	}

}
